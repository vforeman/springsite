// server.js


// set up ============================

var express = require("express");
var logfmt = require("logfmt");
var app = express();

// configuration =====================

app.use(logfmt.requestLogger());

// application add one route (angular will handle the routing ) ================

app.get('/', function(req, res){
	res.sendfile('app/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});



app.configure(function(){
	// look up the static command, pay attention to the effect it has on routing!
	app.use(express.static(__dirname + '/app'));  // set static files location /public/img for users
	app.use(express.logger('dev')); // log every request to the console
	app.use(express.bodyParser()); //pull information from html in POST
	app.use(express.methodOverride()); // simulate DELETE and PUT
});
 

// listen 

var port = Number(process.env.PORT || 5000);
app.listen(port, function(){
	console.log("Listening on " + port);
});