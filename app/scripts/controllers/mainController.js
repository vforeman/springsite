
define(['controllers/controllers', 'services/userService'],
  function(controllers) {
    controllers.controller('mainController', ['$scope', 'UserService',
      function($scope, UserService) {
        $scope.name = UserService.getUser();
        console.log($scope.name);
    }]);
});
