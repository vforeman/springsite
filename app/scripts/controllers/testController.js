define(['controllers/controllers', 'services/userService'],
  function(controllers) {
    controllers.controller('testController', ['$scope', 'UserService',
      function($scope, UserService) {
        $scope.name = UserService.getUser();
    }]);
});
