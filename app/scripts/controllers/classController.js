define(['controllers/controllers', 'services/userService'],
  function(controllers) {
    controllers.controller('classController', ['$scope', 'UserService',
      function($scope, UserService) {
        $scope.name = UserService.getUser();
    }]);
});
