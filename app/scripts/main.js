// the app/scripts/main.js file, which defines our RequireJS config
require.config({
  paths: {
    angular: 'vendor/angular.min',
    jquery: 'vendor/jquery',
    domReady: 'vendor/domReady'
  },
  shim: {
    angular: {
      deps: [ 'jquery'],
      exports: 'angular'
    }
  }
});

require([
  'angular',
  'app',
  'domReady',
  'services/userService',
  'services/classService',
  'controllers/mainController',
  'controllers/testController',
  'controllers/classController',
  'directives/ngbkFocus'
  
  // Any individual controller, service, directive or filter file
  // that you add will need to be pulled in here.
],
  function (angular, app, domReady) {
    'use strict';
    app.config(['$routeProvider',
      function($routeProvider) {
        $routeProvider.when('/', {
          templateUrl: 'views/home.html',
          controller: 'mainController'
        })
        
        $routeProvider.when('/test',{
          templateUrl:"views/test.html",
          controller:'testController'
        })
        // route for the about page

        $routeProvider.when('/staff', {
          templateUrl : 'views/staff.html',
          controller : 'staffController'
        })

        $routeProvider.when('/mission', {
          templateUrl : 'views/mission.html',
          controller : 'missionController'
        })

        $routeProvider.when('/sponsorship', {
          templateUrl : 'views/sponsorship.html',
          controller : 'sponsorshipController'
        })

        $routeProvider.when('/change', {
          templateUrl : 'views/change.html',
          controller : 'changeController'
        })

        $routeProvider.when('/enter', {
          templateUrl : 'views/enter.html',
          controller : 'enterController'
        })

        $routeProvider.when('/contact', {
          templateUrl : 'views/contact.html',
          controller : 'contactController'
        });
      }
    ]);
    domReady(function() {
      angular.bootstrap(document, ['MyApp']);

      // The following is required if you want AngularJS Scenario tests to work
      $('html').addClass('ng-app: MyApp');
    });
  }
);
