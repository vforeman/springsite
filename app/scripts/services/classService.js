define(['services/services'],
  function(services) {
    services.factory('ClassService', [
      function() {
        return {
          getActive: function() {
            return 'testUser';
          }
        };
      }]);
  });
